#!/bin/bash
USER=$(whoami)
#Check to see if KDE5 is installed
if [[ $(sudo pacman -Qs plasma) == ""  && $(sudo pacman -Qs plasma-meta) == "" ]]; then
    sudo pacman -S plasma plasma-meta
fi
if [[ $(pacman -Qs i3session-git) == "" ]]; then
    yaourt -S i3session-git
fi

#Generate a random username
NEW_USER=$(cat /dev/urandom | tr -dc 'a-z' | fold -w 12 | head -n 1)

#create user and add user to basic groups
sudo useradd -m -g users -s /bin/bash $NEW_USER -p password

#save & exit i3 session
i3session save
sudo echo $NEW_USER >> /dev/tty2 >&0 2>&1
sudo echo "password" >> /dev/tty2 >&0 2>&1
chvt 2

#start KDE5
cd /home/$NEW_USER
echo "exec startkde" >> /home/$NEW_USER/.xinitrc

#start KDE5

#su $USER
#delete user
#sudo userdel -r $NEW_USER

#restor i3 session and start i3
#i3session restore

#cd /home/$USER/
#startx
