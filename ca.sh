#!/bin/bash
#usage ./ca example.com

if [[ $1 = ""  ]]; then
    echo "Please run with website name"
    exit 1
fi
echo "mkdir "$1""
mkdir "$1"

echo "cd "$1""
cd "$1"

echo "openssl genrsa -des3 -out "$1"_secure.key 4096"
openssl genrsa -des3 -out "$1"_secure.key 4096

echo "openssl req -new -sha256 -key "$1"_secure.key -out "$1".csr"
openssl req -new -sha256 -key "$1"_secure.key -out "$1".csr

echo "openssl rsa -in "$1"_secure.key -out "$1".key"
openssl rsa -in "$1"_secure.key -out "$1".key

if [[ -f sub.class1.server.sha2.ca.pem ]];then
    echo "Please dowload the signed cert then run this command"
    echo "cat "$1".pem sub.class1.server.sha2.ca.pem > "$1"_chain.pem"
else
    wget https://www.startssl.com/certs/class1/sha2/pem/sub.class1.server.sha2.ca.pem
fi

echo "generating backup certificate and key"
openssl req -new -newkey rsa:4096 -nodes -sha256 -keyout "$1".backup.key -out "$1".backup.csr

echo "HPKP hashes"
echo "(openssl req -inform pem -pubkey -noout < "$1".csr | openssl pkey -pubin -outform der | openssl dgst -sha256 -binary | base64"

First=$(openssl req -inform pem -pubkey -noout < "$1".csr | openssl pkey -pubin -outform der | openssl dgst -sha256 -binary | base64)
echo $First

Second=$(openssl req -inform pem -pubkey -noout < "$1".backup.csr | openssl pkey -pubin -outform der | openssl dgst -sha256 -binary | base64)

echo "(openssl req -inform pem -pubkey -noout < "$1".backup.csr | openssl pkey -pubin -outform der | openssl dgst -sha256 -binary | base64"
echo $Second

echo "server {
    listen 443 ssl spdy;
    listen [::]:443 ssl spdy;
    server_name "$1" www."$1";
    root /home/mediaserver/http/"$1";

    ssl_certificate /etc/nginx/ssl/"$1"/"$1"_chain.pem;
    ssl_certificate_key /etc/nginx/ssl/"$1"/"$1".key;
    add_header Public-Key-Pins 'pin-sha256=\""$First"\";pin-sha256=\""$Second"\";  max-age=15768000;';
    include /etc/nginx/includes/ssl.conf;


    # ... the rest of your configuration
    location / {
    try_files $uri $uri/ =404;
    }
    error_page 404 /404.html;
    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
        root /usr/share/nginx/html;
    }
    include /etc/nginx/includes/php.conf;
    include /etc/nginx/includes/cache.conf;

}

# redirect all http traffic to https
server {
    listen 80;
    listen [::]:80;
    server_name "$1" www."$1";
    return 301 https://\$host\$request_uri;
}" > /etc/nginx/conf.d/$1.conf



